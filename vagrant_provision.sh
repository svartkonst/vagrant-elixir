echo "Adding Erlang Solutions to sources"

echo "deb https://packages.erlang-solutions.com/ubuntu bionic contrib" >> /etc/apt/sources.list

echo "Adding Erlang Solutions keys"
wget https://packages.erlang-solutions.com/ubuntu/erlang_solutions.asc
sudo apt-key add erlang_solutions.asc

echo "Update"
sudo apt-get update 

echo "Installing Erlang ..."
sudo apt-get install -y esl-erlang 

echo "Installing Elixir ..."
sudo apt-get install -y elixir 
